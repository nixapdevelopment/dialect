<?php

return [
    'adminEmail' => 'admin@example.com',
    'maskMoneyOptions' => [
        
    ],
    'lang' => [
        'ro' => [
            'name' => 'Romanian',
        ],
        'ru' => [
            'name' => 'Rumanian',
        ],
    ],
    'displayDateFormat' => 'd.m.Y',
    'dateTimeFormatPHP' => 'd F Y ',
    'dateTimeFormatJS' => 'dd.mm.yyyy',
];

