<?php
namespace app\components\GridView;

use kartik\grid\GridView as KGridView;

class GridView extends KGridView
{
    
    public $tableOptions = ['class' => 'table table-hover table-condensed'];
    
}