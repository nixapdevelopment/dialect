<?php
namespace app\components\JQGrid;

use himiklab\jqgrid\actions\JqGridActiveAction as CJqGridActiveAction;

class JqGridActiveAction extends CJqGridActiveAction
{
    
    protected function renderModelErrors($model)
    {
        $errors = '';
        foreach ($model->errors as $error) {
            $errors .= (implode('', $error) . '<br>');
        }
        echo $errors;
    }
    
}
