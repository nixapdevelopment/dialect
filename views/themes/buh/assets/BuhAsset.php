<?php
namespace app\views\themes\buh\assets;

use yii\web\AssetBundle;


class BuhAsset extends AssetBundle
{
    
    public $sourcePath = '@app/views/themes/buh/assets/files';

    public $css = [
        'css/jquery.formstyler.css',
        'css/jquery.offcanvas.min.css',
        'css/jqgrid.css',
        'css/main.css',
    ];
    
    public $js = [
        'js/jquery.formstyler.min.js',
        'js/jquery.matchHeight-min.js',
        'js/jquery.offcanvas.min.js',
        'js/velocity.min.js',
        'js/scripts.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'rmrevin\yii\fontawesome\AssetBundle'
    ];
}
