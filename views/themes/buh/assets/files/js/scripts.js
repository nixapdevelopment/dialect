$("input.styler, select.styler").styler();



$(document).ready(function(){



    /* EGAL HEIGHT BOX 2*/
    function applyMathcheight1() {

        if ($(window).width() > 768) {
            $(".services-box").matchHeight();
        }
    }

    applyMathcheight1();


    /* EGAL HEIGHT BOX 2*/
    function applyMathcheight2() {

        if ($(window).width() > 768) {
            $(".question-box").matchHeight();
        }
    }

    applyMathcheight2();


    var $el = $("#offcanvas1").offcanvas({
        effect: "slide-in-over",
        overlay: true,
        origin: "left",
        coverage: "340px"
    });

    $(".js-toggle-offcanvas").on("click.offcanvas", function() {
        $el.offcanvas("toggle");
    });


    $('#nav-icon1').click(function(){
        $(this).toggleClass('open');
    });


    $('#slider-menu > li > a').on("click", function(e){
        e.preventDefault();
        $(this).closest('li').find("ul").slideToggle();
        $(this).closest('li').find(">a").toggleClass("active");
    });


    $('.action-box').on("click", function(e){
        e.preventDefault();
        $(this).find(".action-box-dropdown").slideToggle();
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    $('.list-view-results .dropdown-list-block').on("click", function(e){
        e.preventDefault();
        $(this).parent().parent().parent().parent().parent().parent().find(".list-view-dropdown").slideToggle();
        $(this).parent().parent().parent().parent().parent().toggleClass("active");
        $(this).toggleClass("active");
    });


    $('.results-search-list .drop-down-results').on("click", function(e){
        e.preventDefault();
        $(this).parent().parent().parent().parent().parent().find(".results-dropdown-item").slideToggle();
        $(this).parent().parent().parent().parent().toggleClass("active");
        $(this).toggleClass("active");
    });
});



