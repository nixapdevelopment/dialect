<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\HotelSearch */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin([
        'action'=> Url::to('/sejur/avion-search'),
        'method' => 'get',
        'id'=>'search-form',
    ]); ?>


    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div>
                <label >Orase

                    <?= $form->field($model, 'location')->input('text')->label(false)->widget(AutoComplete::className(),[

                        'attribute' => 'location',
                        'options' => [
                            'id' => 'location',
                            'placeholder' => 'Oras',
                            'class' => ''
                        ],
                        'clientOptions' => [
                            'minLength' => 3,
                            'source' => new JsExpression("
                                function(request, response) {
                                    $.getJSON('" . Url::to(['/sejur/auto-complete-locations']) . "', {  location: $('#location').val() }, response);
                                }
                            "),
                        ],
                    ]);
                ?>
                </label>
            </div>
        </div>
        <div class="col-md-8 col-xs-12 col-xs-12">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="calendar-elem">
                        <label > Data plecarii
                      <?= $form->field($model, 'TourDate')->input('text')->label(false)->widget(DatePicker::className(),[

                                'options' => [
                                    //'value' => '22.04.2017',
                                ],
                                'pluginOptions' => [
                                    'format' => Yii::$app->params['dateTimeFormatJS'],
                                    'todayHighlight' => true
                                ]]) ?>

                        </label>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <label > Pret de la :
                        <?= $form->field($model, 'PriceFrom')->input('text',['placeholder' => '199+'])->label(false)?>
                        </label>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    <label > pana la :
                        <?= $form->field($model, 'PriceTo')->input('text',['placeholder' => '-999'])->label(false)?>
                    </label>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6 col-xs-12">
                    <div>

                        <?= Html::submitButton(Yii::t('app', 'Cauta'), ['class' => 'btn btn-primary']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php ActiveForm::end(); ?>