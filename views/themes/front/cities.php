<?php

use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;

Pjax::begin();
echo Html::tag('h2','Cities:');
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_city',
    'pager' => [
        'prevPageLabel' => 'previous',
        'nextPageLabel' => 'next',
        'maxButtonCount' => 5,
    ],

]);

Pjax::end();