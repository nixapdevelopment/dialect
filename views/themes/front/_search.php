<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\jui\AutoComplete;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\HotelSearch */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin([
        'action'=> Url::to('/front/hotel-search'),
        'method' => 'get',
        'id'=>'search-form',
    ]); ?>


<div class="container">
    <div class="filter-tab">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="/front/hotel-search"  data-pjax="false">
                    <span class="fa fa-building"></span>
                    Hoteluri
                </a>
            </li>
            <li role="presentation">
                <a href="/sejur/avion-search"  data-pjax="false">
                    <span class="fa fa-plane"></span>
                    Sejur Avion
                </a>
            </li>
            <li role="presentation">
                <a href="/sejur/bus-search"  data-pjax="false">
                    <span class="fa fa-automobile"></span>
                    Sejur Automobil
                </a>
            </li>
        </ul>
        <div class="content-filter">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="hoteluri">
                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <div>
                                    <?= $form->field($model, 'CountryName')->input('text',['placeholder' => "Oras sau Numele Hotelului",'class'=>''])->label('Oras sau Numele Hotelului')->widget(AutoComplete::className(),[

                                        'attribute' => 'location',
                                        'options' => [
                                            'id' => 'location',
                                            'placeholder' => 'Oras',
                                            'class' => ''
                                        ],
                                        'clientOptions' => [
                                            'minLength' => 3,
                                            'source' => new JsExpression("
                                function(request, response) {
                                    $.getJSON('" . Url::to(['/front/auto-complete-locations']) . "', {  location: $('#location').val() }, response);
                                }
                            "),
                                        ],
                                    ]);
                                    ?>

                                </div>
                            </div>
                            <div class="col-md-8 col-xs-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="calendar-elem">
                                            <label>
                                                Check-in
                                            </label>
                                            <input id="check-in" type="text" class="form-control" placeholder="MM/DD/YY">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="calendar-elem">
                                            <label>
                                                Check-out
                                            </label>
                                            <input id="check-out" type="text" class="form-control" placeholder="MM/DD/YY">
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div>
                                            <label>
                                                Adulti
                                            </label>
                                            <select name="select-person" class="style-select">
                                                <option selected value="Cam. p/t 1 adult">Cam. p/t 1 adult</option>
                                                <option  value="Cam. p/t 1 adult">Cam. p/t 2 adult</option>
                                                <option  value="Cam. p/t 1 adult">Cam. p/t 3 adult</option>
                                                <option  value="Cam. p/t 1 adult">Cam. p/t 4 adult</option>
                                                <option  value="Cam. p/t 1 adult">Cam. p/t 5 adult</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div>

                                            <?= Html::submitButton(Yii::t('app', 'Cauta'), ['class' => 'btn btn-primary']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="sejur-avion">

                </div>
                <div role="tabpanel" class="tab-pane" id="sejur-automobil">

                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>