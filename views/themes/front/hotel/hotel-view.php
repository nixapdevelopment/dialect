<?php

    use yii\helpers\ArrayHelper;
    use yii\bootstrap\Html;
    
?>


<div class="container">
    <h1><?= $hotel['Name'] ?></h1>
    <h4><i class="fa fa-map-marker"></i> <?= $hotel['Address'] ?></h4>
    <div>
        <i class="fa fa-phone"></i> <?= implode(', ', ArrayHelper::map($hotel['Phones'], 'phoneNumber', 'phoneNumber')) ?>
    </div>
    <div class="row">
        <?php foreach ($hotel['Images'] as $image) { ?>
        <div style="padding-bottom: 15px;" class="col-md-2">
            <img width="100%" height="150px;" src="<?= $image['path'] ?>" />
        </div>
        <?php } ?>
    </div>
    <hr />
    <div>
        <div style="border-bottom: 1px solid #ccc;" class="row">
            <div class="col-md-4">
                <b>Tip camera</b>
            </div>
            <div class="col-md-2 text-center">
                <b>Tip masa</b>
            </div>
            <div class="col-md-2 text-center">
                <b>Status</b>
            </div>
            <div class="col-md-2 text-center">
                <b>Tarif total</b>
            </div>
            <div class="col-md-2">
                
            </div>
        </div>
        <?php foreach ($hotel['Prices'] as $price) { ?>
        <div style="border-bottom: 1px solid #ccc; padding: 5px 0;" class="row">
            <div class="col-md-4">
                <b><?= $price['rooms'] ?> X <?= $price['name'] ?> (<?= $price['code'] ?>)</b>
            </div>
            <div class="col-md-2 text-center">
                <?= $price['boardCode'] ?> - <?= $price['boardName'] ?>
            </div>
            <div class="col-md-2 text-center">
                <?= $price['rateType'] ?>
            </div>
            <div class="col-md-2 text-center">
                <?= $price['price'] ?>
            </div>
            <div class="col-md-2 text-center">
                <?= Html::beginForm('/book/accept') ?>
                    <?= Html::hiddenInput('OperatorID', $hotel['OperatorID']) ?>
                    <?= Html::hiddenInput('ExternalID', $hotel['ExternalID']) ?>
                    <?= Html::hiddenInput('RateKey', $price['rateKey']) ?>
                    <?= Html::hiddenInput('RateType', $price['rateType']) ?>
                    <?= Html::hiddenInput('Amount', $price['price']) ?>
                    <?= Html::hiddenInput('Hash', $hotel['hash']) ?>
                    <?= Html::submitButton('Rezerva', [
                        'class' => 'btn btn-success'
                    ]) ?>
                <?= Html::endForm() ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>