<?php

use app\views\themes\front\assets\FrontAsset;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


$bundle = FrontAsset::register($this);

?>

<section class="name-page different-structure">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="title different-title pb5 pt5">
                        <span class="strong-text">
                            Contacte
                        </span>
                </div>
            </div>
            <div class="col-md-6">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="/home/home">
                            ACASA
                        </a>
                    </li>
                    <li class="breadcrumb-item active">
                        CONTACTE
                    </li>
                </ol>
            </div>
        </div>
        <div class="different-logo">
            <img class="img-responsive" src="<?= $bundle->baseUrl?>/images/different-logo.png" alt="">
        </div>
    </div>
</section>

<section class="contacts">
    <div class="container">
        <div class="contacts-group">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="contacts-box">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="icon-box">
                                    <span class="fa fa-map-marker"></span>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="title">
                                    Adresa
                                </div>
                                <div class="text">
                                    str. Stefan cel Mare 126
                                    R. Moldova
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="contacts-box">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="icon-box">
                                        <span>
                                            @
                                        </span>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="title">
                                    Adresa
                                </div>
                                <div class="text">
                                    <a href="#">
                                        info@dialect.com
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="contacts-box">
                        <div class="row">
                            <div class="col-md-3 col-sm-3 col-xs-12">
                                <div class="icon-box">
                                    <span class="fa fa-phone"></span>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-12">
                                <div class="title">
                                    Adresa
                                </div>
                                <div class="text">
                                    <div>
                                        <a href="#">
                                            +373 12 34 5678
                                        </a>
                                    </div>
                                    <div>
                                        <a href="#">
                                            +383 87 65 4321
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt50">
            <div class="col-md-6">
                <div class="map">
                    <div class="title">
                        Locatia noastra
                    </div>
                    <div id="map-3" data-mh="15">

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="contact-form">
                    <div class="title">
                        Lasati-ne un mesaj
                    </div>
                        <div class="row">
                            <?php Pjax::begin([
                                'enablePushState' => false,
                                'enableReplaceState'=>false,

                            ])?>
                            <?php $form = ActiveForm::begin([
                                'action' => ['/site/feedback/feedback/new-feedback'],
                                'method' => 'post',
                                'id'=> 'feedback',
                                'fieldConfig' =>[
                                    'options' => [
                                        'tag' => false,
                                    ]
                                ],
                                'options' => [

                                    'class' => 'feedback-form',
                                    'data-pjax' => true,
                                ]
                            ]); ?>



                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label>
                                   <?= $form->field($feedback, 'Nume',['template' => "{input}"])->input('text',[
                                        'placeholder' => 'Nume',
                                        'class' => '',
                                        'id' => 'inp1',
                                    ])->label(false); ?>
                                </label>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label>
                                    <?= $form->field($feedback, 'Prenume',['template' => "{input}"])->input('text',[
                                        'placeholder' => 'Prenume',
                                        'class' => '',
                                        'id' => 'inp4',
                                    ])->label(false); ?>
                                </label>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label>
                                    <?= $form->field($feedback, 'Email',['template' => "{input}"])->input('text',[
                                        'placeholder' => 'Email',
                                        'class' => '',
                                        'id' => 'inp3'
                                    ])->label(false); ?>
                                </label>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label>
                                    <?= $form->field($feedback, 'Telefon',['template' => "{input}"])->input('text',[
                                        'placeholder' => 'Telefon',
                                        'class' => '',
                                        'id' => 'inp2',
                                    ])->label(false); ?></label>
                            </div>
                            <div class="col-md-12">
                                <label>
                                    <?= $form->field($feedback, 'Mesaj',['template' => "{input}"])->textarea([
                                        'placeholder' => 'Mesaj',
                                        'class' => '',
                                    ])->label(false); ?>
                                </label>
                            </div>
                            <div>
                                <?= Html::submitButton(Yii::t('app', 'Trimite'), ['class' => 'submit-btn btn btn-primary']) ?>
                            </div>
                        </div>



                    <?php ActiveForm::end(); ?>
                    <?php Pjax::end()?>
                </div>
            </div>
        </div>
    </div>
</section>