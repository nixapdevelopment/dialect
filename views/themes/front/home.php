<?php

use app\views\themes\front\assets\FrontAsset;
use yii\bootstrap\Html;
use yii\data\ActiveDataProvider;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


$bundle = FrontAsset::register($this);

    $this->registerJsFile( $bundle->baseUrl . '/js/skills1.js',[
            'depends' => [
                'yii\web\JqueryAsset',
            ]
    ]);
    $this->registerJsFile($bundle->baseUrl . '/js/progressbar.min.js');
?>

<section class="top-slider">
    <div class="swiper-container presentation-slider">
        <div class="swiper-wrapper">
            <?php
            foreach ($sliderItems as $slide){
                ?>

            <div class="swiper-slide">
                <div class="title-slide">
                    <div class="text">
                               <span>
                                   <?= $slide->lang->Title?>
                               </span>
                    </div>
                    <div class="border-fondal">

                    </div>
                </div>
                <a href="<?= Url::to($slide->lang->Link)?>" >
                <?php

                echo Html::img($slide->imageUrl,['class' => 'img-responsive']);
                ?>
                </a>
            </div>

                <?php
            }
            ?>

        </div>
        <!-- Add Arrows -->
        <div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div>
    </div>
</section>
<section class="filter">
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
</section>
<section class="top-destination">
    <div class="container">
        <div class="title">
            Top destinatii
        </div>
        <div class="row">
            <?php foreach ($topDestinations as $destination){ ?>

            <div class="col-md-4">
                <div class="destination-box" data-mh="2">
                    <a href="<?php echo $destination->Link ?>">
                        <div class="img-destination">
                            <?php
                            if ($destination->imagePath){
                                ?>
                                <?= Html::img($destination->imagePath,['alt'=>'','data-mh'=>'1','class'=>'img-responsive'])?>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="title">
                            <?=$destination->lang->Title?>
                            <img src="<?php echo $bundle->baseUrl ?>/images/arrow-right.png" alt="">
                        </div>
                    </a>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</section>
<section class="recent-news">
    <div class="container">
        <div class="title">
            Ultimile noutati
        </div>
        <div class="row">


            <?php
            foreach ($news as $post) {
            ?>

            <div class="col-md-6">
                <div class="recent-news-box">
                    <a href="#">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="black-bg">
                                    <div class="img-box">

                                        <?php
                                        if ($post->mainImage){
                                        ?>
                                        <?= Html::img($post->mainImage->imagePath,['alt'=>'','data-mh'=>'10','class'=>'img-responsive'])?>
                                        <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="description-news-box" data-mh="10">
                                    <div class="date-post">
                                        <?php
                                        echo $post->niceDate;
                                        ?>
                                    </div>
                                    <div class="title-box">
                                        <?php
                                            echo $post->lang->Title;
                                        ?>
                                    </div>
                                    <div class="description">
                                        <?php
                                        echo $post->shortContent;
                                        ?>
                                </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
                <?php
            }
            ?>
        </div>
    </div>
</section>
<section class="subscribe">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="title">
                    Aboneaza-te la newsletter
                </div>
            </div>
            <div class="col-md-6">

                <?php Pjax::begin([
                        'enablePushState' => false,
                        'enableReplaceState'=>false,

                ])?>
                <?php $form = ActiveForm::begin([
                    'action' => ['/site/newsletter/newsletter/add-email'],
                    'method' => 'post',
                    'id'=> 'newsletter',
                    'fieldConfig' =>[
                        'options' => [
                            'tag' => false,
                            ]
                    ],
                    'options' => [

                        'class' => 'subscribe-form',
                        'data-pjax' => true,
                    ]
                ]); ?>

                <?= $form->field($newsletter, 'Email',['template' => "{input}"])->input('text',[
                    'placeholder' => 'Email',
                    'class' => '',

                    ])->label(false); ?>


                    <?= Html::submitButton(Yii::t('app', 'Abonare'), ['class' => 'btn btn-primary']) ?>


                <?php ActiveForm::end(); ?>
                <?php Pjax::end()?>
            </div>
        </div>
    </div>
</section>
<section class="our-skills">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div id="skills1" class="skill-box" data-attribute="<?php echo $countLocations?>">
                    <div class="title-skills">
                        Destinații
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div id="skills2" class="skill-box" data-attribute="<?php echo $countHotels?>">
                    <div class="title-skills">
                        Hoteluri
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div id="skills3" class="skill-box" data-attribute="9999">
                    <div class="title-skills">
                        Turiști mulțumiți
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div id="skills4" class="skill-box" data-attribute="<?php echo $countSejurs?>">
                    <div class="title-skills">
                        Sejururi
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="testimonial">
    <div class="container">
        <div class="title">
            Testimonial
        </div>
        <div class="row">
        <?php
            foreach ($testimonials as $testimonial){
            ?>
                <div class="col-md-6">
                    <div class="testimonial-box" data-mh="11">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="name-user">
                                    <?php echo $testimonial->Name; ?>
                                </div>
                                <div class="company">
                                    <?php echo $testimonial->Function; ?>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="user-opinion">
                                    <?php echo $testimonial->Content; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            }
            ?>


        </div>
    </div>
</section>
<section class="seo">
    <div class="container">
        <div class="title">
            Seo text
        </div>
        <div class="row">
            <?php
            foreach ($seoText as $seoTxt){
            ?>

            <div class="col-md-6">
                <?= $seoTxt->lang->Content?>
            </div>
            <?php
            }
            ?>

        </div>
    </div>
</section>