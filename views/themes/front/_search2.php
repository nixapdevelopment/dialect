<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\HotelSearch */
/* @var $form yii\widgets\ActiveForm */
?>



    <?php $form = ActiveForm::begin([
        'action'=> Url::to('/front/hotel-search'),
        'method' => 'get',
        'id'=>'search-form',
    ]); ?>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="hoteluri">
                    <form action="#" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="calendar-elem">
                                            <label>
                                                Check-in
                                            </label>
                                            <input id="check-in" type="text" class="form-control" placeholder="MM/DD/YY">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="calendar-elem">
                                            <label>
                                                Check-out
                                            </label>
                                            <input id="check-out" type="text" class="form-control" placeholder="MM/DD/YY">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div>
                                            <label>
                                                Adulti
                                            </label>
                                            <select name="select-person" class="style-select">
                                                <option selected value="Cam. p/t 1 adult">Cam. p/t 1 adult</option>
                                                <option  value="Cam. p/t 1 adult">Cam. p/t 2 adult</option>
                                                <option  value="Cam. p/t 1 adult">Cam. p/t 3 adult</option>
                                                <option  value="Cam. p/t 1 adult">Cam. p/t 4 adult</option>
                                                <option  value="Cam. p/t 1 adult">Cam. p/t 5 adult</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div>
                                            <button type="submit">
                                                Cauta
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="sejur-avion">

                </div>
                <div role="tabpanel" class="tab-pane" id="sejur-automobil">

                </div>
            </div>


<?php ActiveForm::end(); ?>