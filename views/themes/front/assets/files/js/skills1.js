/**
 * Created by cristiansavin on 14.08.17.
 */





        var skill1 = $("#skills1").attr("data-attribute");
        var skill2 = $("#skills2").attr("data-attribute");
        var skill3 = $("#skills3").attr("data-attribute");
        var skill4 = $("#skills4").attr("data-attribute");
        var bar = new ProgressBar.Circle(document.getElementById("skills1"), {

            color: '#aaa',
            // This has to be the same size as the maximum width to
            // prevent clipping
            strokeWidth: 4,
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 3400,
            text: {
                autoStyleContainer: false
            },
            from: { color: '#aaa', width: 1 },
            to: { color: '#333', width: 4 },
            // Set default step function for all animate calls
            step: function(state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                var value = Math.round(circle.value() * skill1);
                if (value === 0) {
                    circle.setText('');
                } else {
                    circle.setText(value);
                }

            }
        });
        bar.text.style.fontFamily = '"SF UI Display Thin" , sans-serif';
        bar.text.style.fontSize = '70px';
        bar.text.style.color = '#006ab1';

        bar.animate(1.0);  // Number from 0.0 to 1.0

        var bar2 = new ProgressBar.Circle(document.getElementById("skills2"), {
            color: '#aaa',
            // This has to be the same size as the maximum width to
            // prevent clipping
            strokeWidth: 4,
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            text: {
                autoStyleContainer: false
            },
            from: { color: '#aaa', width: 1 },
            to: { color: '#333', width: 4 },
            // Set default step function for all animate calls
            step: function(state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                var value = Math.round(circle.value() * skill2);
                if (value === 0) {
                    circle.setText('');
                } else {
                    circle.setText(value);
                }

            }
        });
        bar2.text.style.fontFamily = '"SF UI Display Thin" , sans-serif';
        bar2.text.style.fontSize = '70px';
        bar2.text.style.color = '#006ab1';

        bar2.animate(1.0);  // Number from 0.0 to 1.0

        var bar3 = new ProgressBar.Circle(document.getElementById("skills3"), {
            color: '#aaa',
            // This has to be the same size as the maximum width to
            // prevent clipping
            strokeWidth: 4,
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 4400,
            text: {
                autoStyleContainer: false
            },
            from: { color: '#aaa', width: 1 },
            to: { color: '#333', width: 4 },
            // Set default step function for all animate calls
            step: function(state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                var value = Math.round(circle.value() * skill3);
                if (value === 0) {
                    circle.setText('');
                } else {
                    circle.setText(value);
                }

            }
        });
        bar3.text.style.fontFamily = '"SF UI Display Thin" , sans-serif';
        bar3.text.style.fontSize = '70px';
        bar3.text.style.color = '#006ab1';

        bar3.animate(1.0);  // Number from 0.0 to 1.0

        var bar4 = new ProgressBar.Circle(document.getElementById("skills4"), {
            color: '#aaa',
            // This has to be the same size as the maximum width to
            // prevent clipping
            strokeWidth: 4,
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 2400,
            text: {
                autoStyleContainer: false
            },
            from: { color: '#aaa', width: 1 },
            to: { color: '#333', width: 4 },
            // Set default step function for all animate calls
            step: function(state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);
                var value = Math.round(circle.value() * skill4);
                if (value === 0) {
                    circle.setText('');
                } else {
                    circle.setText(value);
                }

            }
        });
        bar4.text.style.fontFamily = '"SF UI Display Thin" , sans-serif';
        bar4.text.style.fontSize = '70px';
        bar4.text.style.color = '#006ab1';

        bar4.animate(1.0);  // Number from 0.0 to 1.0

  