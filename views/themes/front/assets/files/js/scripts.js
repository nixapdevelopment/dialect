var Maper = {
    hotelsMap: function () {
        var map1 = new google.maps.Map(document.getElementById('map-1'), {
            zoom: 13,
            center: {lat: 48.1707422, lng: 27.2683663},
            zoomControl: false,
            disableDoubleClickZoom: true,
            disableDefaultUI: true,
            scrollwheel: false
        });
    },
    hotelMap: function () {
        var map2 = new google.maps.Map(document.getElementById('map-2'), {
            zoom: 13,
            center: {lat: 48.1707422, lng: 27.2683663},
            zoomControl: false,
            disableDoubleClickZoom: true,
            disableDefaultUI: true,
            scrollwheel: false
        });
    },
    contacts_pageMap: function () {
        var map3 = new google.maps.Map(document.getElementById('map-3'), {
            zoom: 13,
            center: {lat: 48.1707422, lng: 27.2683663},
            zoomControl: false,
            disableDoubleClickZoom: true,
            disableDefaultUI: true,
            scrollwheel: false
        });
    }
};

$(document).ready(function(){
    $('a[href="#maps-hotels"]').on('shown.bs.tab', function (e) {
        e.preventDefault();
        Maper.hotelsMap();
    });

    $('a[href="#harta"]').on('shown.bs.tab', function (e) {
        e.preventDefault();
        Maper.hotelMap();
    });


    var swiper = new Swiper('.presentation-slider', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        slidesPerView: 1,
        paginationClickable: true,
        loop: true
    });

    var swiper = new Swiper('.hotel-view', {
        pagination: '.swiper-pagination',
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 50,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev'
    });



    /* HEADER TOGGLE SUBMENU */

    $('.open-submenu').on("click", function(e){
        e.preventDefault();
        $(this).toggleClass("active");
        $("*").not(this).removeClass("active");
        $("ul.toggle-submenu").not($(this).closest("li").find("ul")).slideUp();
        $(this).closest("li").find("ul").slideToggle();
    });

    $('#check-in').datepicker({
        calendarWeeks: true,
        todayHighlight: true
    });
    $('#check-out').datepicker({
        calendarWeeks: true,
        todayHighlight: true
    });

    /* TOGGLE MENU FOR MOBILE */

    $(".open-nav").click(function(){
       $(".top-nav").find("nav").slideToggle();
    });


    /* SELECT 2*/


    $('.style-select').select2();

    /* FORM STYLER */

    $('.style-checkbox').styler();

});

/* VALIDATE FORM */
function validateForm(){
    $(".require-form").validate(
        {
            language: "ro",
            messages: {
                name: {
                    pattern: "Introduceti numele Dvs."
                },
                number: {
                    pattern: "Introduceti numarul de telefon"
                },
                email: {
                    pattern: "Introduceti email-ul Dvs."
                }
            }
        }
    );
    superplaceholder({
        el: inp3,
        sentences: [ 'info@mail.ru', 'info@icloud.com', 'info@gmail.com', 'info@yahoo.com' ],
        options: {
            letterDelay: 390,
            loop: true,
            startOnFocus: false
        }
    });
    superplaceholder({
        el: inp1,
        sentences: [ 'Andrei' ],
        options: {
            letterDelay: 120,
            loop: true,
            startOnFocus: false
        }
    });
    superplaceholder({
        el: inp2,
        sentences: [ '+37369123940' ],
        options: {
            letterDelay: 200,
            loop: true,
            startOnFocus: false
        }
    });
}


/* PRELOADER PAGE */

// var loading_screen = pleaseWait({
//     logo: "",
//     backgroundColor: 'rgba(50,166,243, .8)',
//     loadingHtml: "<div class='title-preloader'>DialectTur</div>" +
//     "<div class='sk-folding-cube'>" +
//     "<div class='sk-cube1 sk-cube'> </div>" +
//     "<div class='sk-cube2 sk-cube'></div> " +
//     " <div class='sk-cube4 sk-cube'> </div> " +
//     "<div class='sk-cube3 sk-cube'></div> </div>"
// });
//
//
// var ele = $(".pg-loading-screen");
// var removeMe = $("body");
// var showMe = $(".wrapper");
// setTimeout(function(){showMe.addClass("show-wrapper")} , 3000);
// setTimeout(function(){removeMe.removeClass("pg-loading")} , 3000);
// setTimeout(function() { ele.hide(); }, 3000);







