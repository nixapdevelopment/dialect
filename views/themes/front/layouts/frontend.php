<?php

use app\views\themes\front\assets\FrontAsset;
use kartik\growl\Growl;
use yii\widgets\Menu;
use yii\helpers\Url;

?>
<?php
$bundle = FrontAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1, user-scalable=0" />
    <title><?= $this->title?></title>
    <?php $this->head() ?>

    <!-- Redirect to http://browsehappy.com/ on IE 8-  -->
    <!--[if lte IE 8]>

    <style type="text/css">
        body{display:none!important;}
    </style>

    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/" />

    <![endif]-->
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrapper">

    <header class="header">
        <div class="header-bar">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 header-bar-border">
                        <div class="header-bar-div">
                            <ul class="ul-inline header-ul-inline">
                                <li><a href="<?= Url::to(['/about']) ?>">Despre DIALECT</a></li>
                                <li><a href="<?= Url::to(['/services']) ?>">Servicii</a></li>
                                <li><a href="<?= Url::to(['/contact']) ?>">Contacte</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 header-bar-border">
                        <div class="header-bar-div text-center">
                            Sună la: <a href="#" class="header-phone">+40-721-123456</a>
                        </div>
                    </div>
                    <div class="col-md-3 header-bar-border">
                        <div class="header-bar-div text-center">
                            Limba: &nbsp; <a href="#">română</a> / &nbsp;<a href="#">english</a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="header-bar-div text-center">
                            Intră în <a href="#" class="header-phone">cont</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo">
                        <a href="/home/home">
                            <img src="<?php echo $bundle->baseUrl ?>/images/logo.png" alt="dialect-tur" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="top-nav">
                        <a href="#" class="open-nav">
                            <span class="fa fa-navicon"></span>
                        </a>
                        <nav>
                            <?php
                            echo Menu::widget([
                                'items' => [
                                    ['label' => 'Acasa', 'url' => ['home/home']],
                                    ['label' => 'Hoteluri', 'url' => ['/hotel-search/hotel-search']],
                                    ['label' => 'Sejur Avion', 'url' => ['sejur/avion-search']],
                                    ['label' => 'Sejur Autocar', 'url' => ['sejur/bus-search']],
                                    ['label' => 'Destinatii Vacanta', 'url' => ['#'],
                                        'options'=>[
                                                'class' => 'open-submenu'
                                        ],
                                        'items' => [
                                        ['label' => 'New Arrivals', 'url' => ['product/index', 'tag' => 'new']],
                                        ['label' => 'Most Popular', 'url' => ['product/index', 'tag' => 'popular']],
                                    ]],
                                    ['label' => 'Oferte Speciale', 'url' => ['#'],
                                        'options' => [
                                            'class' => 'open-submenu'
                                        ],

                                        'items' => [
                                            ['label' => 'New Arrivals', 'url' => ['product/index', 'tag' => 'new']],
                                            ['label' => 'Most Popular', 'url' => ['product/index', 'tag' => 'popular']],
                                        ]],
                                ],
                                'options' => [
                                    'class' => ''
                                ],
                                'submenuTemplate' => "<ul class='toggle-submenu';>{items}</ul>"

                            ]);
                            ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <section>
     <?= $content ?>
    </section>
    <footer>
        <div class="top-line-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="footer-logo">
                                    <a href="index.html">
                                        <img src="<?php echo $bundle->baseUrl ?>/images/logo.png" alt="dialect-tur" class="img-responsive">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="short-description">
                                    În ciuda opiniei publice, Lorem Ipsum nu e un simplu text fără sens.
                                    El îşi are rădăcinile într-o bucată a
                                    literaturii clasice latine
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-md-3 contacts">
                                <div>
                                    <a href="#">
                                        <span class="fa fa-phone"></span>
                                        +373 123 456 789
                                    </a>
                                </div>
                                <div>
                                    <a href="#">
                                        <span class="fa fa-envelope"></span>
                                        info@dialect.com
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 footer-menu">
                                <ul>
                                    <li>
                                        <a href="index.html">
                                            >> Acasa
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            >> Despre noi
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            >> Oferte
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 footer-menu">
                                <ul>
                                    <li>
                                        <a href="index.html">
                                            >> Acasa
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            >> Despre noi
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/contact/help/">
                                            >> Intrebari Frecvente
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3">
                                <div>
                                    <a href="#" class="reservations">
                                        Rezervare
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            &copy;DIALECT by <a href="#">Nixap</a>
        </div>
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>