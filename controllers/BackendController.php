<?php
namespace app\controllers;

use Yii;
use app\controllers\AppController;
use yii\web\NotFoundHttpException;
use app\modules\User\models\User;

class BackendController extends AppController
{
    
    public function init()
    {
        parent::init();
        
        if (Yii::$app->user->isGuest)
        {
            return $this->redirect(['/user/login']);
        }
        
        $this->layout = 'backend';
        
        return $this->checkPermisions();
    }

    public function actionError()
    {
        throw new NotFoundHttpException("Page not found");
    }
    
    public function checkPermisions()
    {
        if (!in_array(Yii::$app->user->identity->Type, [User::TypeAccountant, User::TypeSuperAdmin]))
        {
            return $this->redirect(['/user/login']);
        }
    }
    
}