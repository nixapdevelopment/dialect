<?php

namespace app\modules\Post;

use app\components\Module\SiteModule;

/**
 * post module definition class
 */
class Post extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Post\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
