<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\Clienti\models\Clienti */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clientis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clienti-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'Denumire',
            'CodFiscal',
            'ContAnalitic',
            'Judet',
            'Adresa',
            'ContBancar',
            'Banca',
            'Telefon',
            'Email:email',
            'NrRegistrulComertului',
            'Delegat',
            'CISerie',
            'CINumar',
            'EliberatDe',
            'MijloculDeTransport',
            'Agent',
            'Reducere',
        ],
    ]) ?>

</div>
