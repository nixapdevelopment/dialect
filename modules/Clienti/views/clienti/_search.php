<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\Clienti\models\ClientiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clienti-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Denumire') ?>

    <?= $form->field($model, 'CodFiscal') ?>

    <?= $form->field($model, 'ContAnalitic') ?>

    <?= $form->field($model, 'Judet') ?>

    <?php // echo $form->field($model, 'Adresa') ?>

    <?php // echo $form->field($model, 'ContBancar') ?>

    <?php // echo $form->field($model, 'Banca') ?>

    <?php // echo $form->field($model, 'Telefon') ?>

    <?php // echo $form->field($model, 'Email') ?>

    <?php // echo $form->field($model, 'NrRegistrulComertului') ?>

    <?php // echo $form->field($model, 'Delegat') ?>

    <?php // echo $form->field($model, 'CISerie') ?>

    <?php // echo $form->field($model, 'CINumar') ?>

    <?php // echo $form->field($model, 'EliberatDe') ?>

    <?php // echo $form->field($model, 'MijloculDeTransport') ?>

    <?php // echo $form->field($model, 'Agent') ?>

    <?php // echo $form->field($model, 'Reducere') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
