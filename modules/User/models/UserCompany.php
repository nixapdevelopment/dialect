<?php

namespace app\modules\User\models;

use Yii;
use app\modules\User\models\User;
use app\modules\Company\models\Company;

/**
 * This is the model class for table "UserCompany".
 *
 * @property integer $ID
 * @property integer $UserID
 * @property integer $CompanyID
 *
 * @property Company $company
 * @property User $user
 */
class UserCompany extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'UserCompany';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['UserID', 'CompanyID'], 'required'],
            [['UserID', 'CompanyID'], 'integer'],
            [['CompanyID'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['CompanyID' => 'ID']],
            [['UserID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['UserID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'UserID' => Yii::t('app', 'User ID'),
            'CompanyID' => Yii::t('app', 'Company ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['ID' => 'CompanyID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'UserID']);
    }
}
