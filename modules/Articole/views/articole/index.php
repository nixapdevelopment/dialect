<?php

use kartik\tree\TreeView;
use app\modules\Articole\models\Articole;
use yii\widgets\Pjax;

?>
<br />
<?= TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => Articole::find()->with('childs')->addOrderBy('root, lft'),
    'headingOptions' => ['label' => 'Articole'],
    'fontAwesome' => true,     // optional
    'isAdmin' => false,         // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => true,       // defaults to true
    'cacheSettings' => [        
        'enableCache' => false   // defaults to true
    ],
]); ?>