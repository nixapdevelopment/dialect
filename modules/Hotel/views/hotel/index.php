<?php

use yii\helpers\Html;
use app\components\GridView\GridView;
use yii\widgets\Pjax;
use \app\modules\Location\models\Location;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\Hotel\models\HotelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Hotels');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hotel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'countryID',
                'label' => 'Tara',
                'value' => 'country.lang.Name',
                'filter' => Html::activeDropDownList($searchModel, 'countryID', Location::getList(Yii::t('app', 'Toate'),Location::TypeCountry), ['class' => 'form-control','id '=> 'countryID'])
            ],
            [
                'attribute' => 'regionID',
                'label' => 'Regiune',
                'value' => 'region.lang.Name',
                'filter' => Html::activeDropDownList($searchModel, 'regionID', Location::getList(Yii::t('app', 'Toate'),Location::TypeRegion,$searchModel->countryID), ['class' => 'form-control','id '=> 'regionID'])

            ],
            [
                'attribute' => 'cityID',
                'label' => 'Oras',
                'value' => 'city.lang.Name',
                'filter' => Html::activeDropDownList($searchModel, 'cityID', Location::getList(Yii::t('app', 'Toate'),Location::TypeCity,$searchModel->regionID), ['class' => 'form-control','id '=> 'cityID'])

            ],
            [
                'attribute' => 'Name',
                'label' => 'Hotel',
                'value' => 'lang.Name'

            ],
            [
                'attribute' => 'lang.Address',
                'label' => 'Adresa',

            ],
            [
                'attribute' => 'Stars',
                'label' => 'Stars',
                'value' => 'Stars',
                'filter' => "<div class='row'> <div class='col-sm-6 stars-input' >"
                            .Html::activeDropDownList($searchModel,'starsFrom',['From',1,2,3,4,5],[
                                    'text'=> 'From',
                                    'class'=> 'form-control',
                    ])
                            ."</div> <div class='col-sm-6 stars-input '>"
                            . Html::activeDropDownList($searchModel,'starsTo',['To',1,2,3,4,5],[
                                    'text'=> 'To',
                                    'class'=> 'form-control',
                    ])
                            ."</div></div>",

            ],
            [   'class' => 'yii\grid\ActionColumn',
                'header' => Html::a(Yii::t('app', 'Adauga'), ['create'], ['class' => 'btn btn-success','data-pjax'=> 0]),
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
