<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Hotel\models\Hotel */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Hotel',
]) . $model->lang->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hotels'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="hotel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
