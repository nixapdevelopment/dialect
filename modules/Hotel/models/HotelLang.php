<?php

namespace app\modules\Hotel\models;

use Yii;

/**
 * This is the model class for table "HotelLang".
 *
 * @property integer $ID
 * @property integer $HotelID
 * @property integer $LangID
 * @property string $Name
 * @property string $Description
 * @property string $Address
 *
 * @property Hotel $hotel
 */
class HotelLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'HotelLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['HotelID', 'LangID', 'Name', 'Description', 'Address'], 'required'],
            [['HotelID'], 'integer'],
            [['Description', 'Address'], 'string'],
            [['Name'], 'string', 'max' => 255],
            [['HotelID'], 'exist', 'skipOnError' => true, 'targetClass' => Hotel::className(), 'targetAttribute' => ['HotelID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'HotelID' => Yii::t('app', 'Hotel ID'),
            'LangID' => Yii::t('app', 'Lang ID'),
            'Name' => Yii::t('app', 'Name'),
            'Description' => Yii::t('app', 'Description'),
            'Address' => Yii::t('app', 'Address'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHotel()
    {
        return $this->hasOne(Hotel::className(), ['Hotel.ID' => 'HotelID']);
    }
}
