<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Agenti\models\Agenti */

$this->title = Yii::t('app', 'Create Agenti');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Agentis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agenti-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
