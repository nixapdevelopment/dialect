<?php

namespace app\modules\Agenti\controllers;

use Yii;
use app\modules\Agenti\models\Agenti;
use app\modules\Agenti\models\AgentiSearch;
use app\controllers\BackendController;
use yii\web\NotFoundHttpException;

/**
 * AgentiController implements the CRUD actions for Agenti model.
 */
class AgentiController extends BackendController
{

    public function actionIndex()
    {
        if ($deleteID = (int)Yii::$app->request->get('delete', 0))
        {
            $this->findModel($deleteID)->delete();
            return $this->redirect(['index']);
        }
        
        $searchModel = new AgentiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Agenti model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Agenti model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Agenti();

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            
        }
        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Agenti model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save())
        {
            
        }
        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Agenti model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Agenti the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Agenti::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
