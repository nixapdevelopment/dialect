<?php

namespace app\modules\Blog\controllers;

use app\controllers\SiteBackendController;

/**
 * Default controller for the `blog` module
 */
class DefaultController extends SiteBackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
