<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\Testimonials\models\Testimonials */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Testimonials',
]) . $model->Name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Testimonials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Name, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="testimonials-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
