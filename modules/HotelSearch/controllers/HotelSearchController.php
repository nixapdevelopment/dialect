<?php

namespace app\modules\HotelSearch\controllers;

use Yii;
use app\controllers\FrontController;
use app\modules\Location\models\Location;
use app\modules\Parser\providers\PrestigeProvider;
use app\modules\Parser\providers\HotelbedsProvider;
use app\modules\Booking\models\SearchInfo;

class HotelSearchController extends FrontController
{

    public function actionIndex()
    {
        $selectedLocation = Yii::$app->request->get('LocationID', false) ? Location::find()->with('lang')->where(['ID' => Yii::$app->request->get('LocationID')])->one() : false;
        
        return $this->render('index', [
            'selectedLocation' => $selectedLocation,
        ]);
    }
    
    public function actionAjaxHotelSearch()
    {
        $data = \Yii::$app->request->post();
        unset($data['_csrf']);
        
        $hash = md5(serialize($data));
        
        $searchInfoModel = new SearchInfo([
            'Hash' => $hash,
            'Data' => serialize($data),
        ]);
        $searchInfoModel->save();
        
        Yii::$app->session->set('searchData', $data);
        
        chmod(Yii::getAlias('@app/modules/HotelSearch/cache/'), 0777);
        $cacheFile = Yii::getAlias('@app/modules/HotelSearch/cache/' . $hash . '.txt');
        
        if (file_exists($cacheFile) && filemtime($cacheFile) + 3600 > time())
        {
            $results = unserialize(file_get_contents($cacheFile));
        }
        else
        {
            $provider = new HotelbedsProvider();
            $results = $provider->searchHotels($data, $hash);

            $f = fopen($cacheFile, "a+");
            fwrite($f, serialize($results));
            fclose($f);
            chmod($cacheFile, 0777);
        }
        
        return $this->renderAjax('search-results', [
            'results' => $results,
            'searchID' => $searchInfoModel->ID,
        ]);
    }

}
