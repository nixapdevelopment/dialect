<?php

namespace app\modules\Furnizori\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\Furnizori\models\Furnizori;

/**
 * FurnizoriSearch represents the model behind the search form about `app\modules\Furnizori\models\Furnizori`.
 */
class FurnizoriSearch extends Furnizori
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Judet', 'Agent'], 'integer'],
            [['Denumire', 'CodFiscal', 'ContAnalitic', 'Adresa', 'ContBancar', 'Banca', 'NrRegistrulComertului', 'Telefon', 'Email'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Furnizori::find()->with(['judet', 'agenti']);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'Judet' => $this->Judet,
            'Agent' => $this->Agent,
        ]);

        $query->andFilterWhere(['like', 'Denumire', $this->Denumire])
            ->andFilterWhere(['like', 'CodFiscal', $this->CodFiscal])
            ->andFilterWhere(['like', 'ContAnalitic', $this->ContAnalitic])
            ->andFilterWhere(['like', 'Adresa', $this->Adresa])
            ->andFilterWhere(['like', 'ContBancar', $this->ContBancar])
            ->andFilterWhere(['like', 'Banca', $this->Banca])
            ->andFilterWhere(['like', 'NrRegistrulComertului', $this->NrRegistrulComertului])
            ->andFilterWhere(['like', 'Telefon', $this->Telefon])
            ->andFilterWhere(['like', 'Email', $this->Email]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 15,
            ]
        ]);

        return $dataProvider;
    }
}
