<?php

namespace app\modules\Booking\models;

use Yii;

/**
 * This is the model class for table "SearchInfo".
 *
 * @property integer $ID
 * @property string $Hash
 * @property string $Data
 * @property array $data
 */
class SearchInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'SearchInfo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Hash', 'Data'], 'required'],
            [['Data'], 'string'],
            [['Hash'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Hash' => Yii::t('app', 'Hash'),
            'Data' => Yii::t('app', 'Data'),
        ];
    }
    
    public function getData()
    {
        return unserialize($this->Data);
    }
    
}
