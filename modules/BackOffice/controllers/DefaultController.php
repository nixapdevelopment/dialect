<?php

namespace app\modules\BackOffice\controllers;

use Yii;
use app\controllers\BackendController;
use app\modules\Booking\models\BookingSearch;

/**
 * Default controller for the `backoffice` module
 */
class DefaultController extends BackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new BookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionOrders()
    {
        
    }
}
