<?php

    use kartik\select2\Select2;
    use yii\helpers\Url;
    use yii\web\JsExpression;
    use yii\helpers\ArrayHelper;
    use kartik\date\DatePicker;
    use yii\bootstrap\Html;
    
    $childYears = [];
    for ($y = 1; $y <= 17; $y++)
    {
        $childYears[$y] = $y;
    }

?>
<br />
<div style="min-height: 450px;" class="container">
    <form method="get">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label class="control-label">DESTINATIA TA</label>
                    <?= Select2::widget([
                        'name' => 'LocationID',
                        'options' => ['placeholder' => 'Cautare ...', 'required' => 'reqired'],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 2,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => Url::to(['/site/location/front-ajax/ajax-locations-search']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label">CHECK IN</label>
                    <?= DatePicker::widget([
                        'name' => 'CheckIn',
                        'value' => Yii::$app->request->get('CheckIn'),
                        'removeButton' => false,
                        'pluginOptions' => [
                            'format' => 'dd.mm.yyyy',
                            'todayHighlight' => true,
                            'autoclose' => true,
                        ]
                    ]) ?>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label">NR. NOPTI</label>
                    <input type="number" name="Nights" class="form-control" required value="<?= Yii::$app->request->get('Nights', 1) ?>" min="1" max="30">
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label class="control-label">CAMERE</label>
                    <?= Html::dropDownList('Rooms', Yii::$app->request->get('Rooms'), [
                        1 => 1,
                        2 => 2,
                        3 => 3,
                        4 => 4,
                    ], [
                        'class' => 'form-control'
                    ]) ?>
                </div>
            </div>
        </div>
        <?php for ($i = 1; $i <= 4; $i++) { ?>
        <div <?= $i == 1 || $i <= Yii::$app->request->get("Rooms") ? 'style="display:block;"' : '' ?> class="room-controls">
            <div>Camera <?= $i ?></div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">ADULTI</label>
                        <?= Html::dropDownList("Adults[$i]", Yii::$app->request->get("Adults")[$i], [
                            1 => 1,
                            2 => 2,
                            3 => 3,
                            4 => 4,
                            5 => 5,
                            6 => 6,
                        ], [
                            'class' => 'form-control'
                        ] + ($i != 1 && $i > Yii::$app->request->get("Rooms") ? ['disabled' => 'disabled'] : [])) ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">COPII</label>
                        <?= Html::dropDownList("Childs[$i]", Yii::$app->request->get("Childs")[$i], [
                            0 => 0,
                            1 => 1,
                            2 => 2,
                        ], [
                            'class' => 'form-control'
                        ] + ($i != 1 && $i > Yii::$app->request->get("Rooms") ? ['disabled' => 'disabled'] : [])) ?>
                    </div>
                </div>
                <div class="col-md-3 child-age-control" <?= empty(Yii::$app->request->get("ChildAge")[$i][1]) ? '' : 'style="display:block;"' ?>>
                    <div class="form-group">
                        <label class="control-label">COPIL 1</label>
                        <?= Html::dropDownList("ChildAge[$i][1]", Yii::$app->request->get("ChildAge")[$i][1], $childYears, [
                            'class' => 'form-control'
                        ] + (empty(Yii::$app->request->get("ChildAge")[$i][1]) ? ['disabled' => 'disabled'] : [])) ?>
                    </div>
                </div>
                <div class="col-md-3 child-age-control" <?= empty(Yii::$app->request->get("ChildAge")[$i][2]) ? '' : 'style="display:block;"' ?>>
                    <div class="form-group">
                        <label class="control-label">COPIL 2</label>
                        <?= Html::dropDownList("ChildAge[$i][2]", Yii::$app->request->get("ChildAge")[$i][2], $childYears, [
                            'class' => 'form-control'
                        ] + (empty(Yii::$app->request->get("ChildAge")[$i][2]) ? ['disabled' => 'disabled'] : [])) ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div>
            <button type="submit" class="btn btn-primary">Cautare</button>
        </div>
    </form>
    <?php if (Yii::$app->request->get('LocationID')) { ?>
    <div class="search-hotel-results">
        <h1 class="text-center">Cautare ...</h1>
    </div>
    <?php } ?>
</div>

<style>
    .room-controls, .child-age-control {
        display: none;
    }
</style>

<?php if (Yii::$app->request->get('LocationID')) {
    $this->registerJs("
        $.post('" . Url::to(['/hotel-search/hotel-search/ajax-hotel-search']) . "', " . json_encode(ArrayHelper::merge(Yii::$app->request->get(), [Yii::$app->request->csrfParam => Yii::$app->request->getCsrfToken()])) . ", function(html){
            $('.search-hotel-results').html(html);
            $('#pager-wrap').easyPaginate({
                paginateElement: '.page-item',
                elementsPerPage: 25,
            });
        });
        
        $(document).on('clcik', '.easyPaginateNav a', function(){
            $('html, body').animate({scrollTop: 0}, 500);
        });
        
        $('select[name=LocationID]').append('<option value=\"$selectedLocation->ID\">" . $selectedLocation->lang->Name . "</option>').val($selectedLocation->ID).trigger('change');
    ");
} ?>

<?php $this->registerJs("
    $('select[name=Rooms]').change(function(){
        $('.room-controls').not(':first').hide().find('select').attr('disabled', 'disabled');

        var rooms = parseInt($(this).val());
        
        for (i = rooms - 1; i > 0; i--)
        {
            $('.room-controls').eq(i).show().find('select').removeAttr('disabled');
        }
    });
    
    $('select[name^=Childs]').change(function(){
    $(this).closest('.room-controls').find('.child-age-control').hide().find('select').attr('disabled', 'disabled');
        var childs = parseInt($(this).val());
        for (i = childs - 1; i >= 0; i--)
        {
            $(this).closest('.room-controls').find('.child-age-control').eq(i).show().find('select').removeAttr('disabled');
        }

    });
") ?>