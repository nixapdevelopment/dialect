<?php

use yii\helpers\Html;
use app\components\ActiveForm\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\modules\TipuriDeArticole\models\TipuriDeArticole */
/* @var $form yii\widgets\ActiveForm */
?>

<div id="edit-form-wrap">

    <?php Pjax::begin([
        'id' => 'edit-form-pjax' . md5(microtime(true)),
        'enablePushState' => false,
    ]); ?>

        <?php $form = ActiveForm::begin([
            'id' => 'edit-form-' . md5(microtime(true)),
            'options' => [
                'data-pjax' => true
            ]
        ]); ?>

        <?= $form->field($model, 'Denumire')->textInput(['maxlength' => true]) ?>
    
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'ContArticol')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Diferente')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'TVANeexigibila')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Cheltuieli')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Venituri')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
    
        <div class="row">
            <div class="col-md-2">
                <?= $form->field($model, 'Aprovizionabil')->checkbox() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Consumabil')->checkbox() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Vandabil')->checkbox() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Produs')->checkbox() ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'Nestocat')->checkbox() ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    
    <?php Pjax::end(); ?>

</div>
