<?php

namespace app\modules\Feedback;

use app\components\Module\SiteModule;
/**
 * Feedback module definition class
 */
class Feedback extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Feedback\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
