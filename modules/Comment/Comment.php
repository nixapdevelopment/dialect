<?php

namespace app\modules\Comment;

use app\components\Module\SiteModule;

/**
 * Comment module definition class
 */
class Comment extends SiteModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\Comment\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
