<?php

 use yii\widgets\ListView;
 use yii\bootstrap\Html;
 use yii\data\ActiveDataProvider;
 use yii\bootstrap\ActiveForm;
 use yii\widgets\Pjax;



$this->registerJs(
    '$("document").ready(function(){ 
        $("#form").on("pjax:end", function() {
            $.pjax.reload({container:"#list"});  //Reload GridView
        });
    });'
);

Pjax::begin(['id'=> 'list']);
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => 'commentItem',
]);
Pjax::end()?>



<div class="comment-form">
    <div class="form-group">

</div>
<?php Pjax::begin(['id'=> 'form']);
$form = ActiveForm::begin([
    'options' => ['data-pjax' => true,
                    'id' => md5(microtime()),
    ],

]); ?>


<?= $form->field($newComment, 'Content')->textarea(['rows' => 6])->label("Your comment :") ?>


<div class="form-group">
    <?= Html::submitButton( Yii::t('app', 'Save'),['class' => 'btn btn-success' ]) ?>
</div>

<?php ActiveForm::end();
Pjax::end();
?>

</div>