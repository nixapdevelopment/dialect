var levels = [
    
    {
        background: 'first.png',
        markers: [
            {
                top: 10,
                left: 10,
                icon: 'shop.png',
                id: 1,
            },
            {
                top: 20,
                left: 30,
                icon: 'shop.png',
                id: 2,
            }
        ]
    },
    {
        background: 'second.png',
        markers: [
            {
                top: 10,
                left: 10,
                icon: 'shop.png',
                id: 1,
            },
            {
                top: 20,
                left: 30,
                icon: 'shop.png',
                id: 2,
            },
            {
                top: 56,
                left: 34,
                icon: 'shop.png',
                id: 2,
            }
        ]
    }
];

var MallMaper = {
    
    wrapper: null,
    levels: [],
    
    init: function(wrapper, levels)
    {
        this.wrapper = $(wrapper);
        $.each(levels, function(key, val){
            MallMaper.drawLavel(key, val);
        });
    },
    
    drawLavel: function(key, level)
    {
        this.wrapper.append('<div data-key="' + key + '" onclick="MallMaper.selectLevel(key)" class="level" style="background: ' + level.background + '"></div>');
    },
    
    selectLevel: function(key)
    {
        var currentLevel = $('.level[data-key=' + key + ']');
        $('.level[data-key]').not(currentLevel).fadeOut();
        currentLevel.css({transform: ''});
        this.drawMarkers(key, currentLevel);
    },
    
    drawMarkers: function(levelKey, currentLevel)
    {
        $.each(this.levels[levelKey]['markers'], function(key, val){
            currentLevel.append(`
                <div onclick="MallMaper.showMarkerInfo(val.id)" class="marker" style="top:` + val.top + `%; left: ` + val.left + `%;" data-id="` + val.id + `">
                    <div class="marker-body">
                        <img src="` + val.icon + `" />
                    </div>
                </div>
            `);
        });
    },
    
    showMarkerInfo: function(id)
    {
        $.get('/info.html', {id: id}, function(html){
            $('.marker-info').html(html).show();
        });
    }
    
};

MallMaper.init($('#wrapper'), levels);