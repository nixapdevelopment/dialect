<?php

namespace app\modules\Parser\controllers;

use GuzzleHttp\Client; 
use yii\web\Controller;

class BookingController extends Controller
{
    
    public function actionIndex()
    {
        // создаем экземпляр класса
        $client = new Client();
        // отправляем запрос к странице Яндекса
        $res = $client->request('GET', 'https://www.booking.com/hotel/ro/exclusive-residence-by-the-park-a15.ro.html?aid=304142;label=gen173nr-1DEhJkZXN0aW5hdGlvbmNvdW50cnkowAFCAnJvSCBiBW5vcmVmaI4BiAEBmAEgwgEKd2luZG93cyAxMMgBDNgBA-gBAfgBApICAXmoAgM;sid=9c80986e58e94d5a735014271ea263a9;dest_id=2971;dest_type=region;dist=0;hpos=1;room1=A%2CA;sb_price_type=total;srfid=39b10fc79f5edc759f3f7a89393ef0f31c2499bdX31;srpvid=366823e33b7d0394;type=total;ucfs=1&#hotelTmpl');
        // получаем данные между открывающим и закрывающим тегами body
        $body = $res->getBody();
        // подключаем phpQuery
        $document = \phpQuery::newDocumentHTML($body);
        // получаем список новостей
        
        echo $body;

    }
    
}