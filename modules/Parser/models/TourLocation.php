<?php

namespace app\modules\Parser\models;

use Yii;
use app\modules\Parser\models\Tour;

/**
 * This is the model class for table "TourLocation".
 *
 * @property integer $ID
 * @property integer $TourID
 * @property string $Country
 * @property string $Location
 *
 * @property Tour $tour
 */
class TourLocation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'TourLocation';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TourID', 'Country', 'Location'], 'required'],
            [['TourID'], 'integer'],
            [['Country', 'Location'], 'string', 'max' => 255],
            [['TourID'], 'exist', 'skipOnError' => true, 'targetClass' => Tour::className(), 'targetAttribute' => ['TourID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TourID' => Yii::t('app', 'Tour ID'),
            'Country' => Yii::t('app', 'Country'),
            'Location' => Yii::t('app', 'Location'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tour::className(), ['ID' => 'TourID']);
    }
}
