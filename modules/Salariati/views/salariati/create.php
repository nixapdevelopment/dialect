<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\Salariati\models\Salariati */

$this->title = Yii::t('app', 'Create Salariati');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Salariatis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salariati-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
