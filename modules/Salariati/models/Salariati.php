<?php

namespace app\modules\Salariati\models;

use Yii;
use app\modules\Judet\models\Judet;

/**
 * This is the model class for table "Salariati".
 *
 * @property integer $ID
 * @property string $Nume
 * @property string $Prenume
 * @property integer $PunctDeLucru
 * @property string $Functie
 * @property string $DataAngajarii
 * @property string $Tip
 * @property integer $FunctieBaza
 * @property integer $NormaPeZi
 * @property integer $OrePeLuna
 * @property string $CASIndividuala
 * @property string $CASAngajator
 * @property integer $ContrSomajIndividuala
 * @property integer $ContrSomajAngajator
 * @property integer $ContrFGCS
 * @property integer $ContrAccMunca
 * @property integer $ZileCOAn
 * @property string $TipSalariu
 * @property string $Avans
 * @property string $SalariuBrut
 * @property string $SalariuOrar
 * @property string $CNP
 * @property integer $Judet
 * @property string $Localitate
 * @property string $Strada
 * @property string $Numar
 * @property string $CodPostal
 * @property string $Bloc
 * @property string $Scara
 * @property string $Etaj
 * @property string $Apartament
 * @property string $Sector
 * @property string $Telefon
 * @property string $NrContract
 * @property string $DataContract
 * @property string $CasaDeSanatate
 * @property string $Impozitat
 * @property integer $Pensionar
 * @property integer $FaraContribCCI
 * @property integer $FaraContribSanatateAngajator
 * @property integer $FaraContribSanatateSalariat
 * @property string $TipPlata
 * @property string $ContCard
 * @property string $CodFirma
 * @property string $CISerieNumar
 * @property string $CIEliberatDe
 * @property string $CIEliberatData
 * @property string $Email
 * @property integer $NormaPeZiSpecific
 * @property integer $OrePeLunaSpecific
 *
 * @property Judet $judet
 */
class Salariati extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'Salariati';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nume', 'Prenume', 'Functie', 'DataAngajarii', 'Tip', 'OrePeLuna', 'CASIndividuala', 'CASAngajator', 'ZileCOAn', 'TipSalariu', 'CNP', 'Impozitat', 'TipPlata'], 'required'],
            [['PunctDeLucru', 'FunctieBaza', 'NormaPeZi', 'OrePeLuna', 'ContrSomajIndividuala', 'ContrSomajAngajator', 'ContrFGCS', 'ContrAccMunca', 'ZileCOAn', 'Judet', 'Pensionar', 'FaraContribCCI', 'FaraContribSanatateAngajator', 'FaraContribSanatateSalariat', 'NormaPeZiSpecific', 'OrePeLunaSpecific'], 'integer'],
            [['DataAngajarii', 'DataContract', 'ContCard', 'CodFirma'], 'safe'],
            [['Avans', 'SalariuBrut', 'SalariuOrar'], 'number'],
            [['Nume', 'Prenume', 'Functie', 'Localitate', 'Strada', 'Numar', 'CodPostal', 'Bloc', 'Scara', 'Etaj', 'Apartament', 'Sector', 'Telefon', 'NrContract', 'Email'], 'string', 'max' => 255],
            [['Tip', 'CASIndividuala', 'CASAngajator', 'TipSalariu', 'CNP', 'CasaDeSanatate', 'Impozitat', 'TipPlata', 'CISerieNumar', 'CIEliberatDe', 'CIEliberatData'], 'string', 'max' => 50],
            [['Judet'], 'exist', 'skipOnError' => true, 'targetClass' => Judet::className(), 'targetAttribute' => ['Judet' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Nume' => Yii::t('app', 'Nume'),
            'Prenume' => Yii::t('app', 'Prenume'),
            'PunctDeLucru' => Yii::t('app', 'Punct De Lucru'),
            'Functie' => Yii::t('app', 'Functie'),
            'DataAngajarii' => Yii::t('app', 'Data Angajarii'),
            'Tip' => Yii::t('app', 'Tip'),
            'FunctieBaza' => Yii::t('app', 'Functie Baza'),
            'NormaPeZi' => Yii::t('app', 'Norma Pe Zi'),
            'OrePeLuna' => Yii::t('app', 'Ore Pe Luna'),
            'CASIndividuala' => Yii::t('app', 'CAS individuala'),
            'CASAngajator' => Yii::t('app', 'CAS angajator'),
            'ContrSomajIndividuala' => Yii::t('app', 'Contr Somaj Individuala'),
            'ContrSomajAngajator' => Yii::t('app', 'Contr Somaj Angajator'),
            'ContrFGCS' => Yii::t('app', 'Contr FGCS'),
            'ContrAccMunca' => Yii::t('app', 'Contr. Acc. Munca'),
            'ZileCOAn' => Yii::t('app', 'Zile CO/an'),
            'TipSalariu' => Yii::t('app', 'Tip Salariu'),
            'Avans' => Yii::t('app', 'Avans'),
            'SalariuBrut' => Yii::t('app', 'Salariu Brut'),
            'SalariuOrar' => Yii::t('app', 'Salariu Orar'),
            'CNP' => Yii::t('app', 'CNP'),
            'Judet' => Yii::t('app', 'Judet'),
            'Localitate' => Yii::t('app', 'Localitate'),
            'Strada' => Yii::t('app', 'Strada'),
            'Numar' => Yii::t('app', 'Numar'),
            'CodPostal' => Yii::t('app', 'Cod Postal'),
            'Bloc' => Yii::t('app', 'Bloc'),
            'Scara' => Yii::t('app', 'Scara'),
            'Etaj' => Yii::t('app', 'Etaj'),
            'Apartament' => Yii::t('app', 'Apartament'),
            'Sector' => Yii::t('app', 'Sector'),
            'Telefon' => Yii::t('app', 'Telefon'),
            'NrContract' => Yii::t('app', 'Nr. Contract'),
            'DataContract' => Yii::t('app', 'Data Contract'),
            'CasaDeSanatate' => Yii::t('app', 'Casa De Sanatate'),
            'Impozitat' => Yii::t('app', 'Impozitat'),
            'Pensionar' => Yii::t('app', 'Pensionar'),
            'FaraContribCCI' => Yii::t('app', 'Fara Contrib. CCI'),
            'FaraContribSanatateAngajator' => Yii::t('app', 'Fara Contrib Sanatate Angajator'),
            'FaraContribSanatateSalariat' => Yii::t('app', 'Fara Contrib Sanatate Salariat'),
            'TipPlata' => Yii::t('app', 'Tip Plata'),
            'ContCard' => Yii::t('app', 'Cont Card'),
            'CodFirma' => Yii::t('app', 'Cod Firma'),
            'CISerieNumar' => Yii::t('app', 'Ciserie Numar'),
            'CIEliberatDe' => Yii::t('app', 'Cieliberat De'),
            'CIEliberatData' => Yii::t('app', 'Cieliberat Data'),
            'Email' => Yii::t('app', 'Email'),
            'NormaPeZiSpecific' => Yii::t('app', 'Norma Pe Zi Specific'),
            'OrePeLunaSpecific' => Yii::t('app', 'Ore Pe Luna Specific'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJudet()
    {
        return $this->hasOne(Judet::className(), ['ID' => 'Judet']);
    }
    
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);
        
        $this->DataAngajarii = date('c', strtotime($this->DataAngajarii));
        $this->DataContract = date('c', strtotime($this->DataContract));
        
        return true;
    }
    
}
