<?php

    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\file\FileInput;
    use yii\bootstrap\Tabs;
    use yii\helpers\Url;
    
    if (empty($sliderItem->Image))
    {
        $initialPreview = [];
    }
    else
    {
        $initialPreview[] = Url::to('@web/uploads/slider/' . $sliderItem->Image, true);
    }
    
    $initialPreviewConfig[] = [
        
    ];

?>

<?php $form = ActiveForm::begin([
    'id' => 'save-slider-item-form',
    'action' => Url::to(['save-slider-item'])
]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($sliderItem, 'Image')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
                'pluginOptions' => [
                    'showCaption' => false,
                    'showRemove' => true,
                    'showUpload' => false,
                    'initialPreview' => $initialPreview,
                    'initialPreviewAsData'=>true,
                    'initialPreviewConfig' => $initialPreviewConfig,
                ],
            ]); ?>
            <?= $form->field($sliderItem, 'ID')->hiddenInput(['value' => (int)$sliderItem->ID])->label(false) ?>
            <?= $form->field($sliderItem, 'SliderID')->hiddenInput(['value' => (int)Yii::$app->request->post('sliderID') ])->label(false) ?>
        </div>
        <div class="col-md-8">
            <?php
                $items = [];
                foreach ($sliderItemLangs as $key => $sil)
                {
                    $items[] = [
                        'label' => $key,
                        'content' => $this->render('slider-item-lang', [
                            'sil' => $sil,
                            'key' => $key,
                            'form' => $form
                        ]),
                        'active' => $key == Yii::$app->params['lang']
                    ];
                }
                
                echo Tabs::widget([
                   'items' => $items
                ]); ?>
        </div>
    </div>
    <div>
        <div class="form-group text-right">
            <label style="display: block;" class="control-label">&nbsp;</label>
            <?= Html::button($sliderItem->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-success', 'id' => 'save-slider-item-button']) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>

<style>
    .krajee-default .file-preview-image {
        width: 100% !important;
        height: auto !important;
    }
</style>

<?php $this->registerJs("
    $('#save-slider-item-button').click(function(e){
        $.ajax({
            url: '" . Url::to(['save-slider-item']) . "',
            data: new FormData($('#save-slider-item-form')[0]),
            type: 'post',
            processData: false,
            contentType: false,
            success: function(){
            
                window.location.reload();
            }
        });
    });
") ?>