<?php

use yii\helpers\Html;

/* @var $this yii\web\View */


$this->title = 'Update Slider: ' . $sliderModel->Name;
$this->params['breadcrumbs'][] = ['label' => 'Sliders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $sliderModel->Name;
?>
<div class="slider-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_items', [
        'sliderModel' => $sliderModel,
        'dataProvider' => $dataProvider,
    ]) ?>

</div>
