<?php

namespace app\modules\Slider\controllers;

use Yii;
//use yii\web\Controller;
use app\modules\Slider\models\Slider;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use app\modules\Slider\models\SliderItem;
use app\modules\Slider\models\SliderItemLang;
use app\controllers\SiteBackendController;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `Slider` module
 */
class DefaultController extends SiteBackendController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Slider::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionCreate()
    {
        $sliderModel = new Slider();

        $dataProvider = new ActiveDataProvider([
            'query' =>Slider::find(),
        ]);
        
        if (Yii::$app->request->isPost)
        {
            if ($sliderModel->load(Yii::$app->request->post()) && $sliderModel->validate())
            {
                $sliderModel->save();
                
                return $this->redirect(['update', 'id' => $sliderModel->ID]);
            }
            
        }
        
        return $this->render('create', [
            'sliderModel' => $sliderModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionUpdate($id)
    {
        $sliderModel = Slider::find()->where(['ID' => $id])->one();
        
        if($sliderModel){
            $dataProvider = new ActiveDataProvider([
                'query' => SliderItem::find()->where(['SliderID' => $sliderModel->ID]),
            ]);

            if (Yii::$app->request->isPost)
            {
                if ($sliderModel->load(Yii::$app->request->post()) && $sliderModel->validate())
                {
                    $sliderModel->save();

                    Yii::$app->session->setFlash('success', 'Slider was saved');

                    return $this->redirect(['update', 'id' => $sliderModel->ID]);
                }
            }



            return $this->render('update', [
                'sliderModel' => $sliderModel,
                'dataProvider' => $dataProvider,
            ]);
        }else
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        
    }
    
    public function actionDelete($id)
    {
        Slider::deleteAll(['ID' => $id]);
                
        return $this->redirect(['index']);
    }
    
    public function actionGetItem()
    {
        $id = (int)Yii::$app->request->post('id');
        
        $sliderItem = $id > 0 ? SliderItem::findOne($id) : new SliderItem();
        
        $sliderItemLangs = [];
        foreach (Yii::$app->params['lang'] as $i => $lang)
        {
            $sliderItemLangs[$i] = isset($sliderItem->langs[$i]) ? $sliderItem->langs[$i] : new SliderItemLang([
                'LangID' => $i
            ]);
        }

        return $this->renderAjax('slider-item', [
            'sliderItem' => $sliderItem,
            'sliderItemLangs' => $sliderItemLangs
        ]);
    }
    
    public function actionSaveSliderItem()
    {
        $id = Yii::$app->request->post('SliderItem')['ID'];
        $sliderID = Yii::$app->request->post('SliderItem')['SliderID'];
                
        
        $sliderItem = $id > 0 ? SliderItem::findOne($id) : new SliderItem();
        
        $sliderItemLangs = [];
        foreach (Yii::$app->params['lang'] as $i => $lang)
        {
            $sliderItemLangs[$i] = isset($sliderItem->langs[$i]) ? $sliderItem->langs[$i] : new SliderItemLang([
                'LangID' => $i
            ]);
        }
        
        if ($sliderItem->load(Yii::$app->request->post()))
        {
            $imageIns = \yii\web\UploadedFile::getInstanceByName('SliderItem[Image]');
            $imageName = $imageIns;
            if($imageIns){
            $imageName = md5(microtime(true)) . '.' . $imageIns->extension;
            }
            $imagePath = Yii::getAlias('@webroot/uploads/slider/' . $imageName);
            
            if (!empty($imageIns->extension) && $imageIns->saveAs($imagePath))
            {
                $sliderItem->Image = $imageName;
            }
            
            $sliderItem->SliderID = $sliderID ? $sliderID : 1;
            $sliderItem->save();

            Model::loadMultiple($sliderItemLangs, Yii::$app->request->post());

            foreach ($sliderItemLangs as $sil)
            {
                $sil->SliderItemID = $sliderItem->ID;
                $sil->save();
            }
        }
    }
    
    public function actionDeleteSliderItem($id)
    {
        $model = SliderItem::findOne($id);
        
        SliderItem::deleteAll(['ID' => $id]);
        
        $this->redirect(['/admin/slider/default/update', 'id' => $model->SliderID]);
    }
    
}
